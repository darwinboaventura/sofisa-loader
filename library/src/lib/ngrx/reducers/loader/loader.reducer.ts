import {LoaderTypes} from '../../actions/loader/loader.actions';

export const loaderDefaultState = [];

export function loaderReducer(state = loaderDefaultState, action: any) {
	let loaders;
	let indexOnArray;

	switch (action.type) {
		case LoaderTypes.AddLoaderItem:
			loaders = state.slice(0);
			indexOnArray = loaders.findIndex((element) => {
				return element.page === action.payload.page && element.name === action.payload.name;
			});

			if (indexOnArray === -1) {
				loaders.push(action.payload);
			}

			return loaders;
		case LoaderTypes.RemoveLoaderItem:
			loaders = state.slice(0);
			indexOnArray = loaders.findIndex((element) => {
				return element.page === action.payload.page && element.name === action.payload.name;
			});

			loaders.splice(indexOnArray, 1);

			return loaders;
		default:
			return state;
	}
}


